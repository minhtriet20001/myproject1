import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Form  Demo';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
        ),
        body: const MyCustomForm(),
      ),
    );
  }
}

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Vui lòng nhập Email';
              }
              if (RegExp(r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)$')
                      .hasMatch(value) ==
                  false) {
                return 'Email phải có đủ @ và . ';
              }
              return null;
            },
          ),
          TextFormField(
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Vui lòng nhập Password';
              } else if (RegExp(r'^(?=.*?[A-Z])').hasMatch(value) == false) {
                return 'Password phải có ít nhất một kí tự HOA';
              } else if (RegExp(r'^(?=.*?[a-z])').hasMatch(value) == false) {
                return 'Password phải có ít nhất một chữ cái thường';
              } else if (RegExp(r'^(?=.*?[#?!@$%^&*-])').hasMatch(value) ==
                  false) {
                return 'Password phải có ít một kí tự đặc biệt';
              } else if (RegExp(r'^.{8,}').hasMatch(value) == false) {
                return 'Password phải có chiều dài tối thiểu tám';
              }

              return null;
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Đăng kí thành công')),
                  );
                }
              },
              child: const Text('Đăng kí'),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

class Product {
  final String image, title, description;
  final int price, size, id;
  final Color color;
  Product({
    this.id,
    this.image,
    this.title,
    this.price,
    this.description,
    this.size,
    this.color,
  });
}

List<Product> products = [
  Product(
      id: 1,
      title: "Nike Low 1",
      price: 100,
      size: 12,
      description: dummyText,
      image: "assets/images/giay1.png",
      color: Color(0xFF3D82AE)),
  Product(
      id: 2,
      title: "Áo Jordan",
      price: 50,
      size: 8,
      description: dummyText,
      image: "assets/images/ao.png",
      color: Color(0xFFD3A984)),
  Product(
      id: 3,
      title: "Áo ADLV",
      price: 60,
      size: 10,
      description: dummyText,
      image: "assets/images/ao2.png",
      color: Color(0xFF989493)),
  Product(
      id: 4,
      title: "Giày Off-White",
      price: 88,
      size: 11,
      description: dummyText,
      image: "assets/images/giay2.png",
      color: Color(0xFFE6B398)),
  Product(
      id: 5,
      title: "Quần Levis",
      price: 500,
      size: 12,
      description: dummyText,
      image: "assets/images/quan.png",
      color: Color(0xFFFB7883)),
  Product(
    id: 6,
    title: "Túi Hermes",
    price: 1000,
    size: 12,
    description: dummyText,
    image: "assets/images/tui.png",
    color: Color(0xFFAEAEAE),
  ),
];

String dummyText = "Bộ sưu tập đỉnh cao";
